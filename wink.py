import maabara as ma
import numpy as np
import math

fout=open("winkel.dat", "w")
#          b    y   a   x
data = [[3.6, 3.59,4.7,4.6],
        [3.8, 3.3,4.7, 3.7],
        [2.4, 1.5,4.7,2.8],
        [3.1, 1.45,4.7,2.2],
        [3.7, 1.4,4.5,1.7],
        [1.7, 0.55,4.6,1.5]]

osziwinkel = [

stack = ma.uncertainty.Sheet('a*(1/b)')
stack.set_value('a', error=0.1)
stack.set_value('b', error=0.1)
resulta = stack.batch(data, 'b|a|*|*')
print(resulta)

resulta = np.array(resulta)
resultb = stack.batch(data, '*|*|b|a')
print(resultb)

wink = ma.uncertainty.Sheet('asin(a)')
for i in range(len(resulta)):
    ergeb = np.array(ma.weighted_average([resulta[i],resultb[i]]))
    wink.set_value('a', ergeb[0], error=ergeb[1])
    sinu = np.array(wink.get_result('exact'))
    tbl.add_column( [i, sinu[0], sinu[1]] )
    fout.write(str(i) + "\t" + str(sinu[0]) + "\t" + str(sinu[1]) + "\n")

fout.close()
