reset

set terminal epslatex color

set xrange [-0.5:5.5]
set xlabel "Sekundärspannung $I_2$ [A]"
set ylabel "Phasenverschiebung $\\phi$ [rad]"
set output 'phi.tex'
p "winkel.dat" u 1:2:3 w e t "Lissajous-Figuren", "winkel.dat" u 1:4 t "Oszilloskop", "winkel.dat" u 1:5:6 w e t "Zeigerdiagramm", atan((0.21*sin(pi/2)/((x/8.71)+cos(pi/2)))) / (x>0) lt -1 t "Theorie"
set output
!epstopdf phi.eps
