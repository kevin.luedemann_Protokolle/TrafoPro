\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}
\defcounter {refsection}{0}
\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Theorie}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Unbelasteter Transformator}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Belasteter Transformator}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Lissajous-Figuren}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Durchf\"uhrung}{3}{section.3}
\contentsline {section}{\numberline {4}Auswertung}{3}{section.4}
\contentsline {section}{\numberline {5}Diskussion}{3}{section.5}
\contentsline {section}{\nonumberline Literatur}{4}{section*.3}
