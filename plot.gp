reset

set terminal epslatex color

set xlabel "Primärstrom $I_1$ [A]"
set ylabel "Primärspannung $U_1$ [V]"
set key bottom right

set output "IU1.tex"
p "dat.dat" u 1:2 t "Messwerte"
set output 
!epstopdf IU1.eps
